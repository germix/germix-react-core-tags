import { uuid } from '@germix/germix-react-core';

export interface TagData
{
    id: string,
    value: string,
}

/**
 * @author Germán Martínez
 */
 export class TagStore
{
    tags: TagData[];
    modifiedCallback;

    constructor(tags, modifiedCallback)
    {
        this.tags = [];
        this.modifiedCallback = modifiedCallback;

        for(let i = 0; i < tags.length; i++)
        {
            this.tags.push({
                id: uuid(),
                value: tags[i]
            });
        }
    }
    clear()
    {
        this.tags = [];
        this.modifiedCallback();
    }
    add(value, callback)
    {
        for(let i = 0; i < this.tags.length; i++)
        {
            if(this.tags[i].value === value)
                return;
        }
        const newTag = {
            id: uuid(),
            value: value
        };
        const newTags = [ ...this.tags, newTag ];

        callback(newTag, newTags.length-1);

        this.tags = newTags;
        this.modifiedCallback();
    }
    remove(index)
    {
        let tags = this.tags;

        tags.splice(index, 1);

        this.tags = tags;
        this.modifiedCallback();
    }
    change(index, value)
    {
        let tags: TagData[] = [];

        for(let i = 0; i < this.tags.length; i++)
        {
            if(index !== i)
                tags.push(this.tags[i]);
            else
                tags.push({ id: this.tags[i].id, value: value });
        }

        this.tags = tags;
        this.modifiedCallback();
    }
    getTagValues()
    {
        let ret: string[] = [];
        this.tags.forEach((tag) => {
            ret.push(tag.value);
        });
        return ret;
    }
};
