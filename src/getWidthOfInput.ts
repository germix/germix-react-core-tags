function getWidthOfInput(input, className)
{
    let span = document.createElement('span');
    span.className = className;
    //span.innerHTML = input.value.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;');
    span.innerHTML = input.value;
    
    //document.body.appendChild(span);
    input.parentElement.appendChild(span);
    var theWidth = span.getBoundingClientRect().width;
    input.parentElement.removeChild(span);
    //document.body.removeChild(span);
    return theWidth;
}
export default getWidthOfInput;