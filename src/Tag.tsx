import React from 'react';
import getWidthOfInput from './getWidthOfInput';
import { TagData } from './TagStore';

interface Props
{
    tag: TagData,
    index,
    active,

    onChange(index, value),
    onBlur(event, index),
    onFocus(event, index),
    onKeyDown(event, index),
    onRemove(index),
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class Tag extends React.Component<Props,State>
{
    input;

    render()
    {
        return (
            <div
                key={this.props.index}
                className="tag"
                onClick={ (event) =>
                {
                    event.stopPropagation();
                }}
            >
                <input
                    ref={elem => this.input = elem}
                    type="text"
                    value={this.props.tag.value}
                    className="tag-input"
                    onChange={ (event) =>
                    {
                        this.props.onChange(this.props.index, event.target.value);

                        this.resizeInput();
                    }}

                    onBlur={ (event) =>
                    {
                        this.props.onBlur(event, this.props.index);
                    }}
                    onFocus={ (event) =>
                    {
                        this.props.onFocus(event, this.props.index);
                    }}
                    onKeyDown={ (event) =>
                    {
                       this.props.onKeyDown(event, this.props.index);
                    }}
                />
                <div
                    className="tag-cancel"
                    onClick={ (event) =>
                {
                    this.props.onRemove(this.props.index);
                }}>X</div>
            </div>
        );
    }
    componentDidMount()
    {
        this.resizeInput();
        
        if(this.props.active)
            this.input.focus();
        else
            this.input.blur();
    }
    componentDidUpdate()
    {
        this.resizeInput();
    }
    componentWillReceiveProps(nextProps)
    {
        this.resizeInput();

        if(nextProps.active)
            this.input.focus();
        else
            this.input.blur();
    }
    resizeInput = () =>
    {
        this.input.style.width = getWidthOfInput(this.input, 'tag-input') + 'px';
    }
};
export default Tag;
