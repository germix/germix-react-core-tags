import React from 'react';
import Tag from './Tag';
import { TagStore } from './TagStore';

interface Props
{
    store: TagStore,
}
interface State
{
    currentInputIndex,
}

/**
 * @author Germán Martínez
 */
class Tags extends React.Component<Props,State>
{
    state: State =
    {
        currentInputIndex: null
    }
    constructor(props)
    {
        super(props);
    }
    render()
    {
        let { store } = this.props;

        return (
            <fieldset
                className="tags"
                onClick={ () =>
                {
                    this.createNewTag();
                }}
            >
                { store.tags.map( (tag, index) =>
                {
                    return <Tag
                        key={tag.id}
                        tag={tag}
                        index={index}
                        active={index === this.state.currentInputIndex}
                        
                        onRemove={(index) =>
                        {
                            store.remove(index);
                        }}
                        onChange={(index, value) =>
                        {
                            store.change(index, value);
                        }}
                        onBlur={this.handleBlur}
                        onFocus={this.handleFocus}
                        onKeyDown={this.handleKeyDown}
                        />
                })}
            </fieldset>
        );
    }
    handleBlur = (event, index) =>
    {
        this.setState({
            currentInputIndex: null,
        });
        if(this.props.store.tags[index].value === '')
        {
            this.props.store.remove(index);
        }
    }
    handleFocus = (event, index) =>
    {
        this.setState({
            currentInputIndex: index
        });
    }
    handleKeyDown = (event, index) =>
    {
        const charCode =
            (typeof event.charCode === 'number' && event.charCode !== 0) ? event.charCode : event.keyCode;

        switch(charCode)
        {
            case 37:    // LEFT
                break;
            case 39:    // RIGHT
                break;
            case 8:     // BACKSPACE
                break;
            case 9:     // TAB
                break;
            case 13:    // ENTER
                event.preventDefault();
                event.stopPropagation();
                if(this.state.currentInputIndex !== null)
                {
                    if(this.props.store.tags[this.state.currentInputIndex].value === '')
                    {
                        this.props.store.remove(this.state.currentInputIndex);
                        this.setState({
                            currentInputIndex: null,
                        });
                    }
                }
                this.createNewTag();
                break;
        }
    }
    createNewTag()
    {
        this.props.store.add('', (newTag, newTagIndex) =>
        {
            this.setState({
                currentInputIndex: newTagIndex
            });
        });
    }
};
export default Tags;
