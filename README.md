# Germix React Core - Tags

## About

Germix react core tags components

## Installation

```bash
npm install @germix/germix-react-core-tags
```

## Build

```bash
npm run build
```

## Publish

```bash
npm publish
```

## Build & Publish

```bash
npm run build-publish
```
